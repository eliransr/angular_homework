// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA1h-u1Q0skg9IIOcM9Bl1QTNALNjxw-qg",
    authDomain: "homeworkangular-25c16.firebaseapp.com",
    databaseURL: "https://homeworkangular-25c16.firebaseio.com",
    projectId: "homeworkangular-25c16",
    storageBucket: "homeworkangular-25c16.appspot.com",
    messagingSenderId: "1080356242187",
    appId: "1:1080356242187:web:7cae49d703d59d2653d80e",
    measurementId: "G-P8WXD664B0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
