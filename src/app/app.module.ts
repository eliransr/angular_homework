import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { NavComponent } from './nav/nav.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';

import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';

// Comonents + Services
import { BooksComponent } from './books/books.component';
import { AuthorsComponent } from './authors/authors.component';
import { PostformComponent } from './postform/postform.component';
import { PostsComponent } from './posts/posts.component';
import { EditauthorComponent } from './editauthor/editauthor.component';
//Login  and SugnUp components
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { PostsService } from './posts.service';

// Router
import { RouterModule, Routes } from '@angular/router';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
// Firebase modules
import { AngularFirestoreModule ,AngularFirestore} from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
//Firebase Authentication 
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';



const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors/:authorname/:id', component: AuthorsComponent },
  { path: 'editauthor/:authorname/:id' ,  component: EditauthorComponent},
  { path: 'posts', component: PostsComponent },
  { path: 'postform', component: PostformComponent},
  { path: 'postform/:id', component: PostformComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'login', component: LoginComponent},
  { path: 'classify', component: DocformComponent},
  { path: 'classified', component: ClassifiedComponent},
  { path: '',
  redirectTo: '/books',
  pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    PostformComponent,
    SignUpComponent,
    LoginComponent,
    ClassifiedComponent,
    DocformComponent,
  
  ],
  imports: [
     BrowserModule,
     MatCardModule,
     FormsModule,
     MatExpansionModule,
     BrowserAnimationsModule,
     LayoutModule,
     MatToolbarModule,
     MatButtonModule,
     MatSidenavModule,
     MatIconModule,
     MatListModule,
     MatSelectModule,
     MatInputModule,
     HttpClientModule,
     AngularFirestoreModule,
     AngularFireModule.initializeApp(environment.firebaseConfig, 'HomeWorkAngular'),
     AngularFireStorageModule,
     AngularFireAuthModule,
     RouterModule.forRoot(
      appRoutes,
      {
          // enableTracing: true 
        } // <-- debugging purposes only
     )
   ],
  
  //  providers: [PostsService],
   providers: [AngularFirestore,
               AngularFireAuth],
   bootstrap: [AppComponent]
})
export class AppModule { }
