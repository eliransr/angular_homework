import { HttpClient , HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { AngularFirestore ,AngularFirestoreCollection} from '@angular/fire/firestore';
import { tap, catchError, map, flatMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { BlogPost } from './interfaces/blog-post';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})


export class PostsService {

  // read API
  // private ApiPosts = "https://jsonplaceholder.typicode.com/posts"
  // private ApiUser = "https://jsonplaceholder.typicode.com/users"
 
  UserCollection:AngularFirestoreCollection = this.db.collection('Users');
  PostCollection:AngularFirestoreCollection;
 
 
  constructor(private _http: HttpClient ,private db: AngularFirestore , private authService:AuthService) { }
  
  // Read    
    // Posts = the name of the Collection
  getPosts(userId:string): Observable<any[]> {
    // const ref = this.db.collection('Posts');
    // return ref.valueChanges({idField: 'id'});
    this.PostCollection = this.db.collection(`Users/${userId}/Posts`);
    console.log('Posts collection created');
    return this.PostCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        console.log(data);
        return data;
      }))
    );    
  } 

  // Posts = the name of the Collection
  getPost(userId, id:string):Observable<any>{
    return this.db.doc(`Users/${userId}/Posts/${id}`).get();
  }

  
  
  // Create
      // Posts = the name of the Collection
  addPost(userId:string , title:string, body:string, author:string ){
    console.log('In add posts');
    const post = {title:title , body:body, author:author };
    this.UserCollection.doc(userId).collection('Posts').add(post);
  }
 

  // Update
      // Posts = the name of the Collection
  updatePost(userId:string , id:string ,title:string , body:string ,author:string){
    this.db.doc  (`Users/${userId}/Posts/${id}`).update(
      {
        title:title,
        body:body,
        author:author
      }
    )
  }
  
// Delete
    // Posts = the name of the Collection
  deletePost(userId:string , id:string){
    this.db.doc(`Users/${userId}/Posts/${id}`).delete();
  }
 
}

// ------------------------------------Codes -------------------------------------------------
   
  // read API
  // private ApiPosts = "https://jsonplaceholder.typicode.com/posts"
  // private ApiUser = "https://jsonplaceholder.typicode.com/users"

  // Read
/*    
    // Posts = the name of the Collection
    getPosts(): Observable<any[]> {
      
      const ref = this.db.collection('Posts');
      return ref.valueChanges({idField: 'id'});
    } 
      // Posts = the name of the Collection
    getPost(id:string):Observable<any>{
      return this.db.doc(`Posts/${id}`).get();
    }

 
 // Create
      // Posts = the name of the Collection
      addPost(title:string, body:string, author:string ){
        const post = {title:title , body:body, author:author };
        this.db.collection('Posts').add(post);
      }
    
      // Update
          // Posts = the name of the Collection
      updatePost(id:string ,title:string , body:string ,author:string){
        this.db.doc(`Posts/${id}`).update(
          {
            title:title,
            body:body,
            author:author
          }
        )
      }
      
    // Delete
        // Posts = the name of the Collection
      deletePost(id:string){
        this.db.doc(`Posts/${id}`).delete();
      }
    
      // For read posts by API
      // getPosts()
      // {
      //   return this._http.get<BlogPost[]>(this.ApiPosts); 
      // }
    
      // getUsers()
      // {
      //   return this._http.get<User[]>(this.ApiUser);
      // }
     
    }
     */