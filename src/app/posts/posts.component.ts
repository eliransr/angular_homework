import { PostsService } from './../posts.service'; 
import { Component, OnInit } from '@angular/core';
import { BlogPost } from './../interfaces/blog-post';
import { User } from './../interfaces/user';
import { Observable } from 'rxjs';
import { AuthService } from './../auth.service'; 

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
 
  Posts$:Observable<any[]>;
  userId:string;

  // //For load to API
    // Posts$: BlogPost[]=[];
    // Users$: User[]=[];
    // title:string; 
    // body:string;
    // author:string;
    // message:String;

  // deletePost(id){
  //   this.postsrvice.deletePost(id)
  //   console.log(id);
  // }

  
  deletePost(id){
    this.postsrvice.deletePost(this.userId,id)
    console.log(id);
  }
  
  constructor(private postsrvice:PostsService ,  public authService:AuthService) { }
// //function who upload in the database all the posts
  // saveFunc(){
  //   for (let index = 0; index < this.Posts$.length; index++) {
  //     for (let i = 0; i < this.Users$.length; i++) {
  //       if (this.Posts$[index].userId==this.Users$[i].id) {
  //         this.title = this.Posts$[index].title;
  //         this.body = this.Posts$[index].body;
  //         this.author = this.Users$[i].name;
  //         this.postsrvice.addPosts(this.body, this.author,this.title);
          
  //       }
        
        
  //     }
      
  //   }
  //   this.message ="The data loading was successful"
  // }

  

  
  ngOnInit() {
  //  this.Posts$ = this.postsrvice.getPosts();
 
  // this.postsrvice.getPost()
  // .subscribe(data =>this.Posts$ = data );
  // this.postsrvice.getUsers()
  // .subscribe(data =>this.Users$ = data );
  console.log("NgOnInit started")  
  this.authService.getUser().subscribe(
    user => {
      this.userId = user.uid;
      this.Posts$ = this.postsrvice.getPosts(this.userId); 
    }
  )
    
  }

}
